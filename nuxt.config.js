
var baseURL = process.env.BASE_URL || 'http://localhost:3000'
var isProduction = process.env.VERCEL === `1`
export default {
  server: {
    port: 3000,
    host: '0.0.0.0'
  },
  head: {
    title: 'THORChain.org - Decentralized Liquidity Network',
    meta: [
      { charset: 'utf-8' },
      // {name: 'viewport', content:'width=1500'},
      {name: 'viewport', content:'width=device-width, initial-scale=1.0'},
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>⚡</text></svg>' }
      // <link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>⚡</text></svg>">
    ],
    script: [
      { src: '/js/scaleBasedOnScreenSize.js' } /* file is located at './utils/js/scaleBasedOnScreenSize.js' */
    ],
    base: {target:'_blank'},
  },

  css: [
    '@/assets/styles/main.scss',
  ],

  buildModules: ['@nuxtjs/style-resources', '@nuxtjs/device','@nuxtjs/imagemin', '@nuxtjs/svg'],

  styleResources: {
    scss: [
      './assets/styles/_colors.scss',
      ]
  },

  modules: [
    ['nuxt-i18n', {strategy: 'prefix_and_default', lazy: 'true'}],
    '@nuxtjs/axios',
    "@nuxtjs/svg"
  ],

  plugins: [
    {src:'~/plugins/linksV2.js'},
    {src:'~/plugins/services.js'},
    {src:'~/plugins/google-analytics.js'},
    {src:'@/plugins/youtube.js', ssr: false},
  ],

  i18n: {
    locales: [
      {
        code: 'en',
        name: 'English',
        file: 'en.json'
      },
      {
        code: 'fr',
        name: 'Français',
        file: 'fr.json'
      }
    ],
    defaultLocale: 'en',
    langDir: 'locales/'
  },

  components: {
    dirs: [
      '~/components',
      '~/components/main',
      '~/components/page-components',
      '~/components/sub',
      '~/components/youtube',
      '~/components/dividers/',
      {
        path: '~/components/base/', 
        prefix: 'Base'
      },
    ]
  },
  router: {
    // Run the middleware/switchToMobile.js on every page
    middleware: 'switchToMobile'
  },
  env: {
    serverUrl: process.env.serverUrl,
    baseUrl: baseURL,
    isProduction: isProduction,
    mobileBaseUrl: process.env.MOBILE_BASE_URL,
  },
  axios:{ 
    baseURL: 'http://localhost:3000',
    timeout: 3000
  },
  publicRuntimeConfig: {
    axios: {
      browserBaseURL: baseURL
    }
  },
  privateRuntimeConfig: {
    axios: {
      baseURL: baseURL
    },
  },

  loading: false,
  // loading: {
  //   color: 'rgba(0,0,0,0)',
  //   height: '0px'
  // }
  imagemin: {
    /* module options */
    enableInDev: false
  }
}