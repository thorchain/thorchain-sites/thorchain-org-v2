import Vue from 'vue'
import VueGtag from 'vue-gtag'

var trackingId = null
if(process.env.isProduction){
    trackingId = 'G-QDG7Z69FRZ'     
} else {
    trackingId = 'G-25M8J6EHXX'
}

Vue.use(VueGtag, {
  config: { id: trackingId },
  globalObjectName: 'EventCollector'
})