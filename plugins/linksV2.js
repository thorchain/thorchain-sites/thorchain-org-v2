export default ($context, inject, ) => {

    inject('links', (query) => {
        return byString(links, query)
    })
}

function byString(o, s) { // Reference: https://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-and-arrays-by-string-path
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    var a = s.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
}

var links = {
    // Navigation
    "nav_docs": {
        "label": "nav_docs",
        "category": "navigation",
        "action": "external_link",
        "link": "https://docs.thorchain.org/"
    },
    "nav_telegram_community": {
      "label": "nav_telegram_community",
      "category": "navigation",
      "action": "external_link",
      "link": "https://t.me/thorchain_org"
    },
    "nav_dev_discord_community": {
      "label": "nav_dev_discord_community",
      "category": "navigation",
      "action": "external_link",
      "link": "https://discord.gg/KjPVnGy5jR"
    },
    "nav_gitlab": {
      "label": "nav_gitlab",
      "category": "navigation",
      "action": "external_link",
      "link": "https://gitlab.com/thorchain"
    },
    "nav_twitter": {
      "label": "nav_twitter",
      "category": "navigation",
      "action": "external_link",
      "link": "https://twitter.com/thorchain"
    },

    // Footer
    "footer_lp_forecast_calculator": {
        "label": "lp_forecast_calculator",
        "category": "footer",
        "action": "external_link",
        "link": "https://science.flipsidecrypto.com/thorchain/"
    },
    "footer_immunefi_bug_bounty": {
        "label": "immunefi_bug_bounty",
        "category": "footer",
        "action": "external_link",
        "link": "https://immunefi.com/bounty/thorchain/"
    },
    "footer_thorchain_docs_connecting_to_thorchain": {
        "label": "thorchain_docs_connecting_to_thorchain",
        "category": "footer",
        "action": "external_link",
        "link": "https://docs.thorchain.org/developers/connecting-to-thorchain"
    },
    "footer_docs": {
        "label": "footer_docs",
        "category": "footer",
        "action": "external_link",
        "link": "https://docs.thorchain.org/"
    },
    "footer_blog": {
        "label": "footer_blog",
        "category": "footer",
        "action": "external_link",
        "link": "https://thorchain.medium.com/"
    },
    "footer_runebase": {
        "label": "footer_runebase",
        "category": "footer",
        "action": "external_link",
        "link": "https://www.runebase.org/"
    },

    // External Links
    // /arbitrageurs
    "arbitrageurs_thorchain_docs_how_it_works": {
        "label": "thorchain_docs_how_it_works",
        "category": "arbitrageurs",
        "action": "external_link",
        "link": "https://docs.thorchain.org/roles/trading"
    },
    "arbitrageurs_thorchain_docs_impact_of_liquidity": {
        "label": "thorchain_docs_impact_of_liquidity",
        "category": "arbitrageurs",
        "action": "external_link",
        "link": "https://docs.thorchain.org/roles/trading#impact-of-liquidity"
    },
    "arbitrageurs_medium_trading_competition_results": {
        "label": "medium_trading_competition_results",
        "category": "arbitrageurs",
        "action": "external_link",
        "link": "https://medium.com/thorchain/trading-competition-results-4a1b9fcc0ce5"
    },
    "arbitrageurs_thorchain_docs_connecting_to_thorchain": {
        "label": "thorchain_docs_connecting_to_thorchain",
        "category": "arbitrageurs",
        "action": "external_link",
        "link": "https://docs.thorchain.org/developers/connecting-to-thorchain"
    },
    "arbitrageurs_medium_open_source_bots": {
        "label": "medium_open_source_bots",
        "category": "arbitrageurs",
        "action": "external_link",
        "link": "https://medium.com/thorchain/rune-arb-bot-competition-winners-f7b0897221f0"
    },
    // /arbitrageur-tools
    "arbitrageur_tools_rune_balance_web_app": {
        "label": "rune_balance_web_app",
        "category": "arbitrageur_tools",
        "action": "external_link",
        "link": "https://www.runebalance.com"
    },
    "arbitrageur_tools_rune_balance_twitter": {
        "label": "rune_balance_twitter",
        "category": "arbitrageur_tools",
        "action": "external_link",
        "link": "https://twitter.com/SuperZannah"
    },
    "arbitrageur_tools_rune_balance_telegram": {
        "label": "rune_balance_telegram",
        "category": "arbitrageur_tools",
        "action": "external_link",
        "link": "https://t.me/runebalance"
    },
    // /daos
    "daos_thorchads_web_app": {
        "label": "thorchads_web_app",
        "category": "daos",
        "action": "external_link",
        "link": "https://thorchads.com"
    },
    "daos_thorchads_twitter": {
        "label": "thorchads_twitter",
        "category": "daos",
        "action": "external_link",
        "link": "https://twitter.com/THORChadsDAO"
    },
    "daos_thorchads_telegram": {
        "label": "thorchads_telegram",
        "category": "daos",
        "action": "external_link",
        "link": "https://t.me/thorchadsdao"
    },
    "daos_thorchads_discord": {
        "label": "thorchads_discord",
        "category": "daos",
        "action": "external_link",
        "link": "https://discord.gg/thorchads"
    },
    "daos_thorchads_blog": {
        "label": "thorchads_blog",
        "category": "daos",
        "action": "external_link",
        "link": "https://thorchads.medium.com/"
    },
    "daos_shapeshift_website": {
        "label": "shapeshift_website",
        "category": "daos",
        "action": "external_link",
        "link": "https://shapeshift.com/shapeshift-decentralize-airdrop"
    },
    "daos_shapeshift_proposals": {
        "label": "shapeshift_proposals",
        "category": "daos",
        "action": "external_link",
        "link": "https://app.boardroom.info/shapeshift/proposals"
    },
    "daos_shapeshift_forum": {
        "label": "shapeshift_forum",
        "category": "daos",
        "action": "external_link",
        "link": "https://forum.shapeshift.com"
    },
    "daos_shapeshift_twitter": {
        "label": "shapeshift_twitter",
        "category": "daos",
        "action": "external_link",
        "link": "https://twitter.com/ShapeShift"
    },
    "daos_shapeshift_discord": {
        "label": "shapeshift_discord",
        "category": "daos",
        "action": "external_link",
        "link": "https://discord.com/invite/shapeshift"
    },
    // /developers
    "developers_gitlab": {
        "label": "gitlab",
        "category": "developers",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain"
    },
    "developers_discord": {
        "label": "discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/KjPVnGy5jR"
    },
    "developers_thorchain_docs_integration_overview": {
        "label": "thorchain_docs_integration_overview",
        "category": "developers",
        "action": "external_link",
        "link": "https://dev.thorchain.org/thorchain-dev/wallets/integration-overview"
    },
    "developers_thorchain_docs_connecting_to_thorchain": {
        "label": "thorchain_docs_connecting_to_thorchain",
        "category": "developers",
        "action": "external_link",
        "link": "https://dev.thorchain.org/thorchain-dev/wallets/connecting-to-thorchain"
    },
    "developers_thorchain_docs_transaction_memos": {
        "label": "thorchain_docs_transaction_memos",
        "category": "developers",
        "action": "external_link",
        "link": "https://dev.thorchain.org/thorchain-dev/memos"
    },
    "developers_thorchain_docs_libraries": {
        "label": "thorchain_docs_libraries",
        "category": "developers",
        "action": "external_link",
        "link": "https://dev.thorchain.org/thorchain-dev/libraries"
    },
    "developers_thorchain_docs_seed_service": {
        "label": "thorchain_docs_seed_service",
        "category": "developers",
        "action": "external_link",
        "link": "https://docs.thorchain.org/developers/seed-service"
    },
    "developers_thorchain_docs_chain_clients_overview": {
        "label": "thorchain_docs_chain_clients_overview",
        "category": "developers",
        "action": "external_link",
        "link": "https://docs.thorchain.org/chain-clients/overview"
    },
    "developers_thorchain_docs_utxo_chains": {
        "label": "thorchain_docs_utxo_chains",
        "category": "developers",
        "action": "external_link",
        "link": "https://docs.thorchain.org/chain-clients/utxo-chains"
    },
    "developers_thorchain_docs_evm_chains": {
        "label": "thorchain_docs_evm_chains",
        "category": "developers",
        "action": "external_link",
        "link": "https://docs.thorchain.org/chain-clients/evm-chains"
    },
    "developers_thorchain_docs_bft_chains": {
        "label": "thorchain_docs_bft_chains",
        "category": "developers",
        "action": "external_link",
        "link": "https://docs.thorchain.org/chain-clients/bft-chains"
    },
    "developers_propose_chain_discord": {
        "label": "propose_chain_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/PNJFZg7Wd8"
    },
    "developers_arbitrum_discord": {
        "label": "arbitrum_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/HyBaqQ4zDa"
    },
    "developers_avalanche_discord": {
        "label": "avalanche_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/6hEnMB4PKP"
    },
    "developers_binance_smart_chain_chain_discord": {
        "label": "binance_smart_chain_chain_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/XsBDc2k5K2"
    },
    "developers_dash_discord": {
        "label": "dash_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/W3ENqm36QS"
    },
    "developers_decred_discord": {
        "label": "decred_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/bmmdTtEqHC"
    },
    "developers_doge_coin_discord": {
        "label": "doge_coin_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/PZFMwNwwNy"
    },
    "developers_firo_discord": {
        "label": "firo_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/8X9CtHWkTS"
    },
    "developers_haven_discord": {
        "label": "haven_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/qwpqXHJE3P"
    },
    "developers_ibc_chains_discord": {
        "label": "ibc_chains_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/bfhPpqpc2K"
    },
    "developers_matic_discord": {
        "label": "matic_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/kwC9aFhbxD"
    },
    "developers_monero_discord": {
        "label": "monero_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/jMy9W95ffy"
    },
    "developers_sovryn_discord": {
        "label": "sovryn_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/5ZAxyj2zty"
    },
    "developers_solana_discord": {
        "label": "solana_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/TR6sjakgGU"
    },
    "developers_polkadot_discord": {
        "label": "polkadot_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/tWp9udV3xR"
    },
    "developers_zcash_discord": {
        "label": "zcash_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/gJFkGe8Bpr"
    },
    "developers_zilliqa_discord": {
        "label": "zilliqa_discord",
        "category": "developers",
        "action": "external_link",
        "link": "https://discord.gg/fPcKxYRjnJ"
    },
    // /document-library
    "document_library_github_cryptoeconomics_paper": {
        "label": "github_cryptoeconomics_paper",
        "category": "document_library",
        "action": "external_link",
        "link": "https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-Whitepaper-May2020.pdf"
    },
    "document_library_github_tss_benchmark_ggg18": {
        "label": "github_tss_benchmark_ggg18",
        "category": "document_library",
        "action": "external_link",
        "link": "https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-TSS-Benchmark-July2020.pdf"
    },
    "document_library_github_tss_whitepaper": {
        "label": "github_tss_whitepaper",
        "category": "document_library",
        "action": "external_link",
        "link": "https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-TSS-Paper-June2020.pdf"
    },
    "document_library_github_whitepaper": {
        "label": "github_whitepaper",
        "category": "document_library",
        "action": "external_link",
        "link": "https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-Whitepaper-May2020.pdf"
    },
    "document_library_github_certik_code_review": {
        "label": "github_certik_code_review",
        "category": "document_library",
        "action": "external_link",
        "link": "https://github.com/thorchain/Resources/blob/master/Audits/THORChain-Certik-CodeReview-Mar2020.pdf"
    },
    "document_library_github_gauntlett_economic_security_review": {
        "label": "github_gauntlett_economic_security_review",
        "category": "document_library",
        "action": "external_link",
        "link": "https://github.com/thorchain/Resources/blob/master/Audits/THORChain-Gauntlet-EconomicSecurityReview-May2020.pdf"
    },
    "document_library_github_kudelski_tss_audit": {
        "label": "github_kudelski_tss_audit",
        "category": "document_library",
        "action": "external_link",
        "link": "https://github.com/thorchain/Resources/blob/master/Audits/THORChain-Kudelski-TSS-Audit-June2020.pdf"
    },
    "document_library_github_halborn_security_audit_july_28_2021_september_6_2021": {
        "label": "github_halborn_security_audit_july_28_2021_september_6_2021",
        "category": "document_library",
        "action": "external_link",
        "link": "https://github.com/thorchain/Resources/blob/master/Audits/Halborn-StateMachine-Router-Bifrost-Audit-Sep2021.pdf"
    },
    "document_library_greymatter_research_report": {
        "label": "greymatter_research_report",
        "category": "document_library",
        "action": "external_link",
        "link": "https://greymattercapital.substack.com/p/thorchain-analysis-and-valuation"
    },
    "document_library_mutlicoin_capital_report": {
        "label": "mutlicoin_capital_report",
        "category": "document_library",
        "action": "external_link",
        "link": "https://multicoin.capital/2021/02/23/thorchain-analysis/"
    },
    "document_library_erik_voorhees_report": {
        "label": "erik_voorhees_report",
        "category": "document_library",
        "action": "external_link",
        "link": "https://erikvoorhees.medium.com/an-introduction-to-thorchain-for-bitcoiners-3f621bf0028e"
    },
    "document_library_delphi_digital_report": {
        "label": "delphi_digital_report",
        "category": "document_library",
        "action": "external_link",
        "link": "https://www.delphidigital.io/reports/rune/"
    },
    "document_library_coin_bureau_report": {
        "label": "coin_bureau_report",
        "category": "document_library",
        "action": "external_link",
        "link": "https://www.coinbureau.com/review/thorchain-rune/"
    },
    "document_library_rebase_foundation_report": {
        "label": "rebase_foundation_report",
        "category": "document_library",
        "action": "external_link",
        "link": "https://rebase.foundation/network/thorchain/"
    },
    "document_library_binance_research_report": {
        "label": "binance_research_report",
        "category": "document_library",
        "action": "external_link",
        "link": "https://research.binance.com/en/projects/thorchain"
    },
    "document_library_seba_bank_report": {
        "label": "seba_bank_report",
        "category": "document_library",
        "action": "external_link",
        "link": "https://www.seba.swiss/research/blockchain-interoperability-thorchain"
    },
    "document_library_messari_report": {
        "label": "messari_report",
        "category": "document_library",
        "action": "external_link",
        "link": "https://messari.io/article/thorchain-infrastructure-for-a-multichain-future"
    },
    "document_library_blockfyre_report": {
        "label": "blockfyre_report",
        "category": "document_library",
        "action": "external_link",
        "link": "https://blockfyre.com/rune-thorchain-research-report-review/"
    },
    "document_library_skald_feedback": {
        "label": "skald_feedback",
        "category": "document_library",
        "action": "external_link",
        "link": "https://twitter.com/SKALDcamp"
    },
    // /getting-started
    "getting_started_erik_summary": {
        "label": "erik_summary",
        "category": "getting_started",
        "action": "external_link",
        "link": "https://erikvoorhees.medium.com/an-introduction-to-thorchain-for-bitcoiners-3f621bf0028e"
    },
    // /index aka homepage
    "home_runebase":{
      "label": "runebase",
      "category": "home",
      "action": "external_link",
      "link": "https://www.runebase.org"
    },
    // /insurance
    "insurance_gitlab_protocol_insurance_fund": {
        "label": "gitlab_protocol_insurance_fund",
        "category": "insurance",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/1122"
    },
    "insurance_nexus_mutual_website": {
        "label": "nexus_mutual_website",
        "category": "insurance",
        "action": "external_link",
        "link": "https://nexusmutual.io"
    },
    "insurance_nexus_mutual_buy_cover": {
        "label": "nexus_mutual_buy_cover",
        "category": "insurance",
        "action": "external_link",
        "link": "https://app.nexusmutual.io/cover/buy/get-quote?address=0x0000000000000000000000000000000000000004"
    },
    "insurance_nexus_mutual_protocol_cover": {
        "label": "nexus_mutual_protocol_cover",
        "category": "insurance",
        "action": "external_link",
        "link": "https://nexusmutual.io/pages/ProtocolCoverv1.0.pdf"
    },
    "insurance_nexus_mutual_risk_assessment_resources": {
        "label": "nexus_mutual_risk_assessment_resources",
        "category": "insurance",
        "action": "external_link",
        "link": "https://nexusmutual.gitbook.io/docs/risk-assessment/risk-assessment-resources"
    },
    "insurance_nexus_mutual_cover_pricing": {
        "label": "nexus_mutual_cover_pricing",
        "category": "insurance",
        "action": "external_link",
        "link": "https://nexusmutual.gitbook.io/docs/users/understanding-nexus-mutual/cover-pricing"
    },
    "insurance_nexus_mutual_stake_nxm": {
        "label": "nexus_mutual_stake_nxm",
        "category": "insurance",
        "action": "external_link",
        "link": "https://app.nexusmutual.io/staking/new/deposit-and-stake"
    },
    "insurance_nexus_mutual_statistics": {
        "label": "nexus_mutual_statistics",
        "category": "insurance",
        "action": "external_link",
        "link": "https://nexustracker.io"
    },
    "insurance_nexus_mutual_claims_history": {
        "label": "nexus_mutual_claims_history",
        "category": "insurance",
        "action": "external_link",
        "link": "https://nexusmutual.gitbook.io/docs/claims-assessment/claims-history"
    },
    "insurance_nexus_mutual_distributor_contract": {
        "label": "nexus_mutual_distributor_contract",
        "category": "insurance",
        "action": "external_link",
        "link": "https://github.com/NexusMutual/smart-contracts/blob/master/docs/DISTRIBUTOR.md"
    },
    // /interfaces
    "interfaces_thorswap_web_app": {
        "label": "interfaces_thorswap_web_app",
        "category": "thorswap_web_app",
        "action": "external_link",
        "link": "https://app.thorswap.finance"
    },
    "interfaces_thorswap_testnet_web_app": {
        "label": "thorswap_testnet_web_app",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://testnet.thorswap.finance"
    },
    "interfaces_thorswap_stagenet_web_app": {
        "label": "thorswap_stagenet_web_app",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://stagenet.thorswap.finance"
    },
    "interfaces_thorswap_desktop_app": {
        "label": "thorswap_desktop_app",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://thorswap.finance/desktop/"
    },
    "interfaces_thorswap_twitter": {
        "label": "thorswap_twitter",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/THORSwap"
    },
    "interfaces_thorswap_discord": {
        "label": "thorswap_discord",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://discord.gg/thorswap"
    },
    "interfaces_thorswap_blog": {
        "label": "thorswap_blog",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://thorswap.medium.com"
    },
    "interfaces_asgardex_desktop_app": {
        "label": "asgardex_desktop_app",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://github.com/thorchain/asgardex-electron/releases"
    },
    "interfaces_asgardex_twitter": {
        "label": "asgardex_twitter",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/asgardex"
    },
    "interfaces_asgardex_discord": {
        "label": "asgardex_discord",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://discord.gg/Fg9kAsQSb2"
    },
    "interfaces_asgardex_github": {
        "label": "asgardex_github",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://github.com/thorchain/asgardex-electron"
    },
    "interfaces_skip_web_app": {
        "label": "skip_web_app",
        "category": "interfaces",
        "action": "external_link",
        "link": process.env.isProduction ? "https://app.skip.exchange" : "https://skipexchange-splash-git-develop-skipexchange.vercel.app"
    },
    "interfaces_skip_testnet_web_app": {
        "label": "skip_testnet_web_app",
        "category": "interfaces",
        "action": "external_link",
        "link": process.env.isProduction ? "https://testnet.app.skip.exchange" : "https://testnet-skipexchange-git-develop-skipexchange.vercel.app"
    },
    "interfaces_skip_twitter": {
        "label": "skip_twitter",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/SKIPswap"
    },
    "interfaces_skip_telegram": {
        "label": "skip_telegram",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://t.me/SKIPexchange"
    },
    "interfaces_skip_discord": {
        "label": "skip_discord",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://discord.gg/WzBKKG2XSN"
    },
    "interfaces_skip_github": {
        "label": "skip_github",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://github.com/SKIPexchange/asgard-exchange"
    },
    "interfaces_brokkr_web_app": {
        "label": "brokkr_web_app",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://brokkr.finance"
    },
    "interfaces_brokkr_twitter": {
        "label": "brokkr_twitter",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/BrokkrFinance"
    },
    "interfaces_brokkr_telegram": {
        "label": "brokkr_telegram",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://t.me/Brokkrfeedback"
    },
    "interfaces_shapeshift_mobile_app": {
        "label": "shapeshift_mobile_app",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://shapeshift.app.link/thorchain"
    },
    "interfaces_shapeshift_web_app": {
        "label": "shapeshift_web_app",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://app.shapeshift.com"
    },
    "interfaces_shapeshift_twitter": {
        "label": "shapeshift_twitter",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/ShapeShift"
    },
    "interfaces_shapeshift_discord": {
        "label": "shapeshift_discord",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://discord.com/invite/shapeshift"
    },
    "interfaces_thorwallet_website": {
        "label": "thorwallet_website",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://thorwallet.org"
    },
    "interfaces_thorwallet_web_app": {
        "label": "thorwallet_web_app",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://app.thorwallet.org"
    },
    "interfaces_thorwallet_whitepaper": {
        "label": "thorwallet_whitepaper",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://www.thorwallet.org/assets/documents/Thorwallet_Pitch_2022_Q1.pdf"
    },
    "interfaces_thorwallet_twitter": {
        "label": "thorwallet_twitter",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/ThorWallet"
    },
    "interfaces_thorwallet_telegram": {
        "label": "thorwallet_telegram",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://t.me/THORWalletOfficial"
    },
    "interfaces_thorwallet_blog": {
        "label": "thorwallet_blog",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://thorwallet.medium.com/"
    },
    "interfaces_thorwallet_github": {
        "label": "thorwallet_github",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://github.com/THORWallet/"
    },
    "interfaces_defispot_web_app": {
        "label": "defispot_web_app",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://www.defispot.com/"
    },
    "interfaces_defispot_twitter": {
        "label": "defispot_twitter",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/Defi_Spot"
    },
    "interfaces_defispot_telegram": {
        "label": "defispot_telegram",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://t.me/DefiSpot"
    },
    "interfaces_defispot_discord": {
        "label": "defispot_discord",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://discord.com/invite/YzcXSjtE"
    },
    "interfaces_rangoexchange_website": {
        "label": "rangoexchange_website",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://rango.exchange"
    },
    "interfaces_rangoexchange_twitter": {
        "label": "rangoexchange_twitter",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/RangoExchange"
    },
    "interfaces_rangoexchange_telegram": {
        "label": "rangoexchange_telegram",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://t.me/rangoexchange"
    },
    "interfaces_rangoexchange_blog": {
        "label": "rangoexchange_blog",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/RangoExchange"
    },
    "interfaces_dragonsdex_twitter": {
        "label": "dragonsdex_twitter",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/DragonsDex"
    },
    "interfaces_ferz_web_app": {
        "label": "interfaces_ferz_web_app",
        "category": "ferz_web_app",
        "action": "external_link",
        "link": "https://app.ferz.com"
    },
    "interfaces_ferz_chrome_ext": {
        "label": "interfaces_ferz_chrome_ext",
        "category": "ferz_chrome_ext",
        "action": "external_link",
        "link": "https://chrome.google.com/webstore/detail/ferz-wallet/anfhjfflodcjcfmgcgfenjlkkehphhal"
    },
    "interfaces_ferz_twitter": {
        "label": "ferz_twitter",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://twitter.com/ferzwallet"
    },
    "interfaces_ferz_telegram": {
        "label": "ferz_telegram",
        "category": "interfaces",
        "action": "external_link",
        "link": "https://t.me/ferzwallet"
    },
    // /liquidity-providers
    "liquidity_providers_thorchain_docs_compensation": {
        "label": "thorchain_docs_compensation",
        "category": "liquidity_providers",
        "action": "external_link",
        "link": "https://docs.thorchain.org/roles/liquidity-providers"
    },
    "liquidity_providers_thorchain_docs_factors_affecting_yield": {
        "label": "thorchain_docs_factors_affecting_yield",
        "category": "liquidity_providers",
        "action": "external_link",
        "link": "https://docs.thorchain.org/roles/liquidity-providers#factors-affecting-yield"
    },
    "liquidity_providers_thorchain_docs_how_it_works": {
        "label": "thorchain_docs_how_it_works",
        "category": "liquidity_providers",
        "action": "external_link",
        "link": "https://docs.thorchain.org/roles/liquidity-providers#how-it-works"
    },
    "liquidity_providers_thorchain_docs_requirements_costs": {
        "label": "thorchain_docs_requirements_costs",
        "category": "liquidity_providers",
        "action": "external_link",
        "link": "https://docs.thorchain.org/roles/liquidity-providers#requirements-costs"
    },
    "liquidity_providers_lp_forecast_calculator": {
        "label": "lp_forecast_calculator",
        "category": "liquidity_providers",
        "action": "external_link",
        "link": "https://science.flipsidecrypto.com/thorchain/"
    },
    "liquidity_providers_thorswap_web_app": {
        "label": "thorswap_web_app",
        "category": "liquidity_providers",
        "action": "external_link",
        "link": "https://thorswap.finance"
    },
    "liquidity_providers_asgardex_web_app": {
        "label": "asgardex_web_app",
        "category": "liquidity_providers",
        "action": "external_link",
        "link": "https://github.com/thorchain/asgardex-electron/releases"
    },
    "liquidity_providers_skip_web_app": {
        "label": "skip_web_app",
        "category": "liquidity_providers",
        "action": "external_link",
        "link": process.env.isProduction ? "https://app.skip.exchange" : "https://skipexchange-git-develop-skipexchange.vercel.app"
    },
    "liquidity_providers_tbnb_rune_faucet_bot": {
        "label": "tbnb_rune_faucet_bot",
        "category": "liquidity_providers",
        "action": "external_link",
        "link": "https://t.me/runefaucetbot"
    },
    "liquidity_providers_skip_testnet_web_app": {
        "label": "skip_testnet_web_app",
        "category": "interfaces",
        "action": "external_link",
        "link": process.env.isProduction ? "https://testnet.app.skip.exchange" : "https://testnet-skipexchange-git-develop-skipexchange.vercel.app"
    },
    "liquidity_providers_lp_university_discord": {
        "label": "lp_university_discord",
        "category": "liquidity_providers",
        "action": "external_link",
        "link": "https://discord.gg/GdFYhM4Nnu"
    },
    // /liquidity-provider-tools
    "liquidity_provider_tools_lp_forecast_calculator_web_app": {
        "label": "lp_forecast_calculator_web_app",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://science.flipsidecrypto.com/thorchain/"
    },
    "liquidity_provider_tools_lp_forecast_calculator_discord": {
        "label": "lp_forecast_calculator_discord",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://flipsidecrypto.com/discord"
    },
    "liquidity_provider_tools_lp_forecast_calculator_twitter": {
        "label": "lp_forecast_calculator_twitter",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://twitter.com/flipsidecrypto"
    },
    "liquidity_provider_tools_lp_university_discord": {
        "label": "lp_university_discord",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://discord.gg/GdFYhM4Nnu"
    },
    "liquidity_provider_tools_lp_university_twitter": {
        "label": "lp_university_twitter",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://twitter.com/THORChainLPU"
    },
    "liquidity_provider_tools_thoryield_web_app": {
        "label": "thoryield_web_app",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://thoryield.com"
    },
    "liquidity_provider_tools_thoryield_twitter": {
        "label": "thoryield_twitter",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://twitter.com/THORYieldApp"
    },
    "liquidity_provider_tools_decentralfi_web_app": {
        "label": "decentralfi_web_app",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://decentralfi.io"
    },
    "liquidity_provider_tools_decentralfi_twitter": {
        "label": "decentralfi_twitter",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://twitter.com/decentralfi"
    },
    "liquidity_provider_tools_decentralfi_telegram": {
        "label": "decentralfi_telegram",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://t.me/decentralfi"
    },
    "liquidity_provider_tools_decentralfi_gitlab": {
        "label": "decentralfi_gitlab",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://gitlab.com/decentralfi/decentralfi.io"
    },
    "liquidity_provider_tools_decentralfi_blog": {
        "label": "lp_university_decentralfi_blog",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://decentralfi.medium.com/"
    },
    "liquidity_provider_tools_thorchain_live_chaosnet_web_app": {
        "label": "thorchain_live_chaosnet_web_app",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://thorchain.live/thorchain/chaosnet"
    },
    "liquidity_provider_tools_thorchain_live_testnet_web_app": {
        "label": "thorchain_live_testnet_web_app",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://thorchain.live/thorchain/testnet/"
    },
    "liquidity_provider_tools_thor_infobot_telegram": {
        "label": "thor_infobot_telegram",
        "category": "liquidity_provider_tools",
        "action": "external_link",
        "link": "https://t.me/thor_infobot"
    },
    // /network
    "network_thorchain_docs_incentive_pendulum": {
        "label": "thorchain_docs_incentive_pendulum",
        "category": "network",
        "action": "external_link",
        "link": "https://docs.thorchain.org/how-it-works/incentive-pendulum"
    },
    "network_thorchain_docs_emissions": {
        "label": "thorchain_docs_emissions",
        "category": "network",
        "action": "external_link",
        "link": "https://docs.thorchain.org/how-it-works/emission-schedule"
    },
    "network_google_docs_emission_schedule": {
        "label": "google_docs_emission_schedule",
        "category": "network",
        "action": "external_link",
        "link": "https://docs.google.com/spreadsheets/d/1e5A7TaV6CZtdVqlOSuXSSY7UYiRW9yzd1ST6QTZNqLw/edit#gid=918223980"
    },
    "network_thorchain_docs_fees": {
        "label": "thorchain_docs_fees",
        "category": "network",
        "action": "external_link",
        "link": "https://docs.thorchain.org/how-it-works/fees"
    },
    "network_thorchain_docs_prices": {
        "label": "thorchain_docs_prices",
        "category": "network",
        "action": "external_link",
        "link": "https://docs.thorchain.org/how-it-works/prices"
    },
    "network_thorchain_docs_governance": {
        "label": "thorchain_docs_governance",
        "category": "network",
        "action": "external_link",
        "link": "https://docs.thorchain.org/how-it-works/governance"
    },
    "network_discord": {
        "label": "discord",
        "category": "network",
        "action": "external_link",
        "link": "https://discord.gg/KjPVnGy5jR"
    },
    // /network-explorers
    "network_explorers_viewblock_mccn": {
        "label": "viewblock_mccn",
        "category": "network_explorers",
        "action": "external_link",
        "link": "https://v2.viewblock.io/thorchain"
    },
    "network_explorers_viewblock_stagenet": {
        "label": "viewblock_mctn",
        "category": "network_explorers",
        "action": "external_link",
        "link": "https://v2.viewblock.io/thorchain?network=stagenet"
    },
    "network_explorers_thorchain_explorer": {
        "label": "thormon_web_app",
        "category": "network_explorers",
        "action": "external_link",
        "link": "https://thorchain.net"
    },
    "network_explorers_thorchain_alerts_bot_telegram": {
        "label": "thorchain_alerts_bot_telegram",
        "category": "network_explorers",
        "action": "external_link",
        "link": "https://twitter.com/THOR_InfoBot"
    },
    "network_explorers_thorchain_bot_twitter": {
        "label": "thorchain_bot_twitter",
        "category": "network_explorers",
        "action": "external_link",
        "link": "https://twitter.com/thor_bot"
    },
    // /nft-collections
    "nft_collections_thorchain_collectibles_opensea": {
        "label": "thorchain_collectibles_opensea",
        "category": "nft_collections",
        "action": "external_link",
        "link": "https://opensea.io/collection/thorchain-collectibles"
    },
    "nft_collections_thorguards_website": {
        "label": "thorguards_website",
        "category": "nft_collections",
        "action": "external_link",
        "link": "https://www.thorguards.com"
    },
    "nft_collections_thorguards_twitter": {
        "label": "thorguards_twitter",
        "category": "nft_collections",
        "action": "external_link",
        "link": "https://twitter.com/ThorGuards"
    },
    "nft_collections_thorguards_discord": {
        "label": "thorguards_discord",
        "category": "nft_collections",
        "action": "external_link",
        "link": "https://discord.gg/ThorGuards"
    },
    "nft_collections_pixelthor_opensea": {
        "label": "pixelthor_opensea",
        "category": "nft_collections",
        "action": "external_link",
        "link": "https://opensea.io/collection/pixelthor"
    },
    "nft_collections_pixelthor_twitter": {
        "label": "pixelthor_twitter",
        "category": "nft_collections",
        "action": "external_link",
        "link": "https://twitter.com/thorchainnft"
    },
    "nft_collections_pixelthor_telegram": {
        "label": "pixelthor_telegram",
        "category": "nft_collections",
        "action": "external_link",
        "link": "https://t.me/runenft"
    },
    "nft_collections_thorchain_punks_opensea": {
        "label": "thorchain_punks_opensea",
        "category": "nft_collections",
        "action": "external_link",
        "link": "https://opensea.io/collection/thorchainpunks"
    },
    "nft_collections_thorchain_punks_telegram": {
        "label": "thorchain_punks_telegram",
        "category": "nft_collections",
        "action": "external_link",
        "link": "https://t.me/thorpunks"
    },
    "nft_collections_roon_twitter": {
        "label": "roon_twitter",
        "category": "nft_collections",
        "action": "external_link",
        "link": "https://twitter.com/THORChainPlus/status/1396588068354863107?s=20"
    },
    // /node-operators
    "node_operators_thorchain_docs_incentive_pendulum": {
        "label": "thorchain_docs_incentive_pendulum",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/how-it-works/incentive-pendulum"
    },
    "node_operators_thorchain_docs_emissions": {
        "label": "thorchain_docs_emissions",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/how-it-works/emission-schedule"
    },
    "node_operators_danxia_capital_vault_nodes": {
        "label": "danxia_capital_vault_nodes",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://twitter.com/Danxia_Capital/status/1439003703374139394?s=20"
    },
    "node_operators_gitlab_vault_nodes": {
        "label": "gitlab_vault_nodes",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/1012"
    },
    "node_operators_thornode_chaosnet_announcements_discord": {
        "label": "hornode_chaosnet_announcements_discord",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://discord.gg/KjPVnGy5jR"
    },
    "node_operators_thormon_web_app": {
        "label": "thormon_web_app",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://thorchain.network"
    },
    "node_operators_viewblock_ygg_vaults_mccn": {
        "label": "viewblock_ygg_vaults_mccn",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://viewblock.io/thorchain/vaults"
    },
    "node_operators_thorchain_docs_thornode_overview": {
        "label": "thorchain_docs_thornode_overview",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thornodes/overview"
    },
    "node_operators_thorchain_docs_cluster_launcher": {
        "label": "thorchain_docs_cluster_launcher",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thornodes/kubernetes"
    },
    "node_operators_thorchain_docs_deploying": {
        "label": "thorchain_docs_deploying",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thornodes/deploying"
    },
    "node_operators_thorchain_docs_joining": {
        "label": "thorchain_docs_joining",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thornodes/joining"
    },
    "node_operators_thorchain_docs_managing": {
        "label": "thorchain_docs_managing",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thornodes/managing"
    },
    "node_operators_thorchain_docs_alerting": {
        "label": "thorchain_docs_alerting",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thornodes/alerting"
    },
    "node_operators_thorchain_docs_leaving": {
        "label": "thorchain_docs_leaving",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thornodes/leaving"
    },
    "node_operators_thorchain_docs_emergency_procedures": {
        "label": "thorchain_docs_emergency_procedures",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thornodes/emergency-procedures"
    },
    "node_operators_thorchain_docs_checklist": {
        "label": "thorchain_docs_checklist",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thornodes/checklist"
    },
    "node_operators_discord": {
        "label": "discord",
        "category": "node_operators",
        "action": "external_link",
        "link": "https://discord.gg/KjPVnGy5jR"
    },
    // /project-funding
    "project_funding_thorchain_dev_discord": {
        "label": "thorchain_dev_discord",
        "category": "project_funding",
        "action": "external_link",
        "link": "https://discord.gg/S9eAnJuhT6"
    },
    "project_funding_thorchain_blog": {
        "label": "thorchain_blog",
        "category": "project_funding",
        "action": "external_link",
        "link": "https://medium.com/thorchain"
    },
    "project_funding_thorstarter_website": {
        "label": "thorstarter_website",
        "category": "project_funding",
        "action": "external_link",
        "link": "https://thorstarter.org"
    },
    "project_funding_thorstarter_twitter": {
        "label": "thorstarter_twitter",
        "category": "project_funding",
        "action": "external_link",
        "link": "https://twitter.com/thorstarter"
    },
    "project_funding_thorstarter_telegram": {
        "label": "thorstarter_telegram",
        "category": "project_funding",
        "action": "external_link",
        "link": "https://t.me/thorstarter"
    },
    "project_funding_thorstarter_discord": {
        "label": "thorstarter_discord",
        "category": "project_funding",
        "action": "external_link",
        "link": "https://discord.com/invite/fPjbPxm37F"
    },
    "project_funding_thorstarter_blog": {
        "label": "thorstarter_blog",
        "category": "project_funding",
        "action": "external_link",
        "link": "https://thorstarter.medium.com"
    },
    "project_funding_thorchain_community_telegram": {
        "label": "thorchain_community_telegram",
        "category": "project_funding",
        "action": "external_link",
        "link": "https://t.me/thorchain_org"
    },
    // /research-labs
    "research_labs_gitlab_order_book": {
        "label": "gitlab_order_book",
        "category": "research_labs",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/1364"
    },
    "research_labs_thorchain_gitlab_economic_design": {
        "label": "thorchain_gitlab_economic_design",
        "category": "research_labs",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/1255"
    },
    "research_labs_thorchain_gitlab_lending_desing": {
        "label": "thorchain_gitlab_lending_design",
        "category": "research_labs",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/1412"
    },
    "research_labs_twitter_protocol_owned_liquidity": {
        "label": "twitter_protocol_owned_liquidity",
        "category": "research_labs",
        "action": "external_link",
        "link": "https://twitter.com/ninerealms_cap/status/1447998742976557056?s=20"
    },
    "research_labs_gitlab_protocol_insurance_fund": {
        "label": "gitlab_protocol_insurance_fund",
        "category": "research_labs",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/1122"
    },
    "research_labs_gitlab_vault_nodes": {
        "label": "gitlab_vault_nodes",
        "category": "research_labs",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/1012"
    },
    "research_labs_thorchain_gitlab_composite_model": {
        "label": "thorchain_gitlab_composite_model",
        "category": "research_labs",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/796"
    },
    // /rune
    "rune_thorchain_docs_rune_overview": {
        "label": "thorchain_docs_rune_overview",
        "category": "rune",
        "action": "external_link",
        "link": "https://docs.thorchain.org/rune"
    },
    "rune_thorchain_docs_rune_liquidity": {
        "label": "thorchain_docs_rune_liquidity",
        "category": "rune",
        "action": "external_link",
        "link": "https://docs.thorchain.org/rune#1.-liquidity"
    },
    "rune_thorchain_docs_rune_security": {
        "label": "thorchain_docs_rune_security",
        "category": "rune",
        "action": "external_link",
        "link": "https://docs.thorchain.org/rune#2.-security"
    },
    "rune_thorchain_docs_rune_governance": {
        "label": "thorchain_docs_rune_governance",
        "category": "rune",
        "action": "external_link",
        "link": "https://docs.thorchain.org/rune#3.-governance"
    },
    "rune_thorchain_docs_rune_incentives": {
        "label": "thorchain_docs_rune_incentives",
        "category": "rune",
        "action": "external_link",
        "link": "https://docs.thorchain.org/rune#4.-incentives"
    },
    "rune_thorchain_docs_emissions": {
        "label": "thorchain_docs_emissions",
        "category": "rune",
        "action": "external_link",
        "link": "https://docs.thorchain.org/how-it-works/emission-schedule"
    },
    "rune_google_docs_emission_schedule": {
        "label": "google_docs_emission_schedule",
        "category": "rune",
        "action": "external_link",
        "link": "https://docs.google.com/spreadsheets/d/1e5A7TaV6CZtdVqlOSuXSSY7UYiRW9yzd1ST6QTZNqLw/edit#gid=918223980"
    },
    // /security-solvency
    "security_solvency_halborn_update": {
        "label": "halborn_update",
        "category": "security_solvency",
        "action": "external_link",
        "link": "https://twitter.com/HalbornSecurity/status/1445470526839746560"
    },
    "security_solvency_immunefi_bug_bounty": {
        "label": "immunefi_bug_bounty",
        "category": "security_solvency",
        "action": "external_link",
        "link": "https://immunefi.com/bounty/thorchain/"
    },
    "security_solvency_thorchain_network_verify_solvency": {
        "label": "thorchain_network_verify_solvency",
        "category": "security_solvency",
        "action": "external_link",
        "link": "https://thorchain.network"
    },
    // /technology
    "technology_thorchain_docs_technology_overview": {
        "label": "thorchain_docs_technology_overview",
        "category": "technology",
        "action": "external_link",
        "link": "https://docs.thorchain.org/technology"
    },
    "technology_thorchain_docs_technology_bifrost": {
        "label": "thorchain_docs_technology_bifrost",
        "category": "technology",
        "action": "external_link",
        "link": "https://docs.thorchain.org/technology#the-bifroest-protocol-1-way-state-pegs"
    },
    "technology_thorchain_docs_technology_state_machine": {
        "label": "thorchain_docs_technology_state_machine",
        "category": "technology",
        "action": "external_link",
        "link": "https://docs.thorchain.org/technology#thorchain-state-machine"
    },
    "technology_thorchain_docs_technology_signer": {
        "label": "thorchain_docs_technology_signer",
        "category": "technology",
        "action": "external_link",
        "link": "https://docs.thorchain.org/technology#signer-bifroest"
    },
    "technology_thorchain_docs_technology_vaults": {
        "label": "thorchain_docs_technology_vaults",
        "category": "technology",
        "action": "external_link",
        "link": "https://docs.thorchain.org/technology#thorchain-vaults"
    },
    "technology_thorchain_network_verify_solvency": {
        "label": "thorchain_network_verify_solvency",
        "category": "technology",
        "action": "external_link",
        "link": "https://thorchain.network"
    },
    "technology_viewblock_verify_vaults": {
        "label": "viewblock_verify_vaults",
        "category": "technology",
        "action": "external_link",
        "link": "https://viewblock.io/thorchain/vaults"
    },
    // /thorfi
    "thorfi_thorchain_docs_thorfi_liquidity_model": {
        "label": "thorchain_docs_thorfi_liquidity_model",
        "category": "thorfi",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thorchain-finance/continuous-liquidity-pools"
    },
    "thorfi_thorchain_docs_thorfi_synthetic_model": {
        "label": "thorchain_docs_thorfi_synthetic_model",
        "category": "thorfi",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thorchain-finance/synthetic-asset-model"
    },
    "thorfi_thorchain_docs_thorfi_lending_model": {
        "label": "thorchain_docs_thorfi_lending_model",
        "category": "thorfi",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thorchain-finance/lending-model"
    },
    "thorfi_thorchain_docs_thorfi_composite_model": {
        "label": "thorchain_docs_thorfi_composite_model",
        "category": "thorfi",
        "action": "external_link",
        "link": "https://docs.thorchain.org/thorchain-finance/composite-model"
    },
    "thorfi_flyacro_thread_economic_forces": {
        "label": "flyacro_thread_economic_forces",
        "category": "thorfi",
        "action": "external_link",
        "link": "https://twitter.com/flyacro/status/1439675371100704772"
    },
    "thorfi_lp_university": {
        "label": "lp_university",
        "category": "thorfi",
        "action": "external_link",
        "link": "https://discord.gg/GdFYhM4Nnu"
    },
    "thorfi_lp_university_introduction": {
        "label": "thorfi_lp_university_introduction",
        "category": "thorfi",
        "action": "external_link",
        "link": "https://crypto-university.medium.com/introduction-to-thorfinance-thorfi-7012d826833e"
    },
    "thorfi_economic_design": {
        "label": "thorfi_economic_design",
        "category": "thorfi",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/1255"
    },
    // /thornames
    "name_service_thornames_web_app": {
        "label": "thornames_web_app",
        "category": "name_service",
        "action": "external_link",
        "link": "https://thorname.com"
    },
    "name_service_thorswap": {
        "label": "thorswap",
        "category": "name_service",
        "action": "external_link",
        "link": "https://thorswap.finance"
    },
    "name_service_xdefi": {
        "label": "xdefi",
        "category": "name_service",
        "action": "external_link",
        "link": "https://www.xdefi.io"
    },
    "name_service_thorwallet": {
        "label": "thorwallet",
        "category": "name_service",
        "action": "external_link",
        "link": "https://thorwallet.org"
    },
    "name_service_skip": {
        "label": "skip",
        "category": "name_service",
        "action": "external_link",
        "link": process.env.isProduction ? "https://app.skip.exchange" : "https://skipexchange-splash-git-develop-skipexchange.vercel.app"
    },
    "name_service_asgardex": {
        "label": "asgardex",
        "category": "name_service",
        "action": "external_link",
        "link": "https://github.com/thorchain/asgardex-electron/releases"
    },
    "name_service_shft_cntrl_bullish_thornames": {
        "label": "shft_cntrl_bullish_thornames",
        "category": "name_service",
        "action": "external_link",
        "link": "https://twitter.com/Shft_Cntrl/status/1407841337412669440?s=20"
    },
    "name_service_thorswap_discord": {
        "label": "thorswap_discord",
        "category": "name_service",
        "action": "external_link",
        "link": "https://discord.gg/thorswap"
    },
    // /wallets
    "wallets_xdefi_browser_extension": {
        "label": "xdefi_browser_extension",
        "category": "wallets",
        "action": "external_link",
        "link": "https://www.xdefi.io"
    },
    "wallets_xdefi_twitter": {
        "label": "xdefi_twitter",
        "category": "wallets",
        "action": "external_link",
        "link": "https://twitter.com/xdefi_wallet"
    },
    "wallets_xdefi_telegram": {
        "label": "xdefi_telegram",
        "category": "wallets",
        "action": "external_link",
        "link": "https://t.me/xdefi_io"
    },
    "wallets_xdefi_discord": {
        "label": "xdefi_discord",
        "category": "wallets",
        "action": "external_link",
        "link": "https://discord.com/invite/7sXXy823Kb"
    },
    "wallets_thorchain_dev_hardware_wallets_discord": {
        "label": "thorchain_dev_hardware_wallets_discord",
        "category": "wallets",
        "action": "external_link",
        "link": "https://discord.gg/h8tMVb6zZM"
    },
    // /recaps/weekly
    // /dec-15-21-2021
    "weekly_recap_dev_update_thorchain_mainnet_discussion_dec_15_21_2021": {
        "label": "dev_update_thorchain_mainnet_discussion_dec_15_21_2021",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://twitter.com/THORNOOBs/status/1470465552963436547"
    },
    "weekly_recap_dev_update_bond_rewards_relative_bond_size": {
        "label": "dev_update_bond_rewards_relative_bond_size",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/1187"
    },
    "weekly_recap_dev_update_performance_primary_churn_factor": {
        "label": "dev_update_performance_primary_churn_factor",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/issues/1184"
    },
    "weekly_recap_dev_update_mimir_controls_node_operators": {
        "label": "dev_update_mimir_controls_node_operators",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/merge_requests/2023"
    },
    "weekly_recap_dev_update_chaosnet_v0_76_launched": {
        "label": "dev_update_chaosnet_v0_76_launched",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/devops/node-launcher/-/merge_requests/387"
    },
    "weekly_recap_dev_update_stagenet_is_coming": {
        "label": "dev_update_stagenet_is_coming",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/thornode/-/merge_requests/2019"
    },
    "weekly_recap_dev_update_zec_integration_in_works": {
        "label": "dev_update_zec_integration_in_works",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/devops/node-launcher/-/merge_requests/383"
    },
    "weekly_recap_dev_update_chain_id_for_easier_fork": {
        "label": "dev_update_chain_id_for_easier_fork",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://gitlab.com/thorchain/devops/node-launcher/-/issues/58"
    },
    "weekly_recap_questions_asked_when_mainnet": {
        "label": "questions_asked_when_mainnet",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://twitter.com/THORNOOBs/status/1470465552963436547"
    },
    "weekly_recap_feedback_received_lending_collateral_in_thorfi": {
        "label": "feedback_received_lending_collateral_in_thorfi",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://twitter.com/tbr90/status/1467746026836611074"
    },
    "weekly_recap_feedback_response_lending_collateral_in_thorfi": {
        "label": "feedback_response_lending_collateral_in_thorfi",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://twitter.com/THORNOOBs/status/1471259393899384832"
    },
    "weekly_recap_feedback_received_thorswap_inefficient_liquidity_pools": {
        "label": "feedback_received_thorswap_inefficient_liquidity_pools",
        "category": "weekly_recap",
        "action": "external_link",
        "link": "https://twitter.com/THORNOOBs/status/1469586733566025730"
    },
    "highlights_thorchain_monthly_stats_november": {
        "label": "thorchain_monthly_stats_november",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/THORNOOBs/status/1468279593408684037"
    },
    "highlights_thorchain_whats_next": {
        "label": "thorchain_whats_next",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/THORNOOBs/status/1469618613749358593"
    },
    "highlights_thorchain_infobot_block_minute_is_live": {
        "label": "thorchain_infobot_block_minute_is_live",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/tirinox/status/1470729076403019779"
    },
    "highlights_thorchain_pie_chart_showing_asset_liquidity_share": {
        "label": "thorchain_pie_chart_showing_asset_liquidity_share",
        "category": "highlights",
        "action": "external_link",
        "link": "https://docs.google.com/spreadsheets/d/1mc1mBBExGxtI5a85niijHhle5EtXoTR_S5Ihx808_tM/edit#gid=1034754950"
    },
    "highlights_trust_wallet": {
        "label": "highlights_trust_wallet",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/TrustWallet/status/1603031998498590721"
    },
    "highlights_savers": {
        "label": "highlights_savers",
        "category": "highlights",
        "action": "external_link",
        "link": "https://medium.com/thorchain/thorchain-savers-vaults-fc3f086b4057"
    },
    "highlights_thorchain_layers_defences": {
        "label": "highlights_thorchain_layers_defences",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/THORChain/status/1583596416597131264"
    },
    "highlights_thorchain_dev_update_118-119": {
        "label": "thorchain_dev_update_118-119",
        "category": "highlights",
        "action": "external_link",
        "link": "https://medium.com/thorchain/dev-update-118-119-92ccfa56920e"
    },
    "highlights_thorchain_dev_update_139-140": {
        "label": "thorchain_dev_update_139-140",
        "category": "highlights",
        "action": "external_link",
        "link": "https://medium.com/thorchain/dev-update-139-140-5ffcb482d3f2"
    },
    "highlights_thorwallet_web_app": {
        "label": "thorwallet_web_app",
        "category": "highlights",
        "action": "external_link",
        "link": "https://medium.com/thorchain/dev-update-140-143-1944a3a33d70"
    },
    "highlights_thorchain_dev_update_140-143": {
        "label": "thorchain_dev_update_140-143",
        "category": "highlights",
        "action": "external_link",
        "link": "https://thorwallet.medium.com/webapp-beta-launch-19d58ffda386"
    },
    "highlights_kraken_listed": {
        "label": "kraken_listed",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/krakenfx/status/1522591141635502080"
    },
    "highlights_community_new_thorchain_wallpaper_degencreation": {
        "label": "community_new_thorchain_wallpaper_degencreation",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/degencreation/status/1469761473945743365"
    },
    "highlights_thorchain_protocol_processes_1m_swaps": {
        "label": "thorchain_protocol_processes_1m_swaps",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/anthonybayss/status/1468786392540934148"
    },
    "highlights_0xventuresdao_why_rune_price_differs": {
        "label": "0xventuresdao_why_rune_price_differs",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/0x_Ventures/status/1469343787663171588"
    },
    "highlights_xdefi_collaboration_coingecko_nft_giveaway": {
        "label": "xdefi_collaboration_coingecko_nft_giveaway",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/xdefi_wallet/status/1471030322544484359"
    },
    "highlights_xdefi_version_10_0_released": {
        "label": "xdefi_version_10_0_released",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/xdefi_wallet/status/1470703195416584195"
    },
    "highlights_xdefi_archon_0x_joins_as_content_writer": {
        "label": "xdefi_archon_0x_joins_as_content_writer",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/xdefi_wallet/status/1470347170742214668"
    },
    "highlights_xdefi_proposal_pools_pylon_protocol_passed": {
        "label": "xdefi_proposal_pools_pylon_protocol_passed",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/xdefi_wallet/status/1469676678808932356"
    },
    "highlights_ama_gotitbykojiro_and_defispot_dec_6_2021": {
        "label": "ama_gotitbykojiro_and_defispot_dec_6_2021",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/xdefi_wallet/status/1467826552117334027"
    },
    "highlights_xdefi_voxie_nft_contest": {
        "label": "xdefi_voxie_nft_contest",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/xdefi_wallet/status/1468597743979573251"
    },
    "highlights_xdefi_hiring_scrum_master": {
        "label": "xdefi_hiring_scrum_master",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/goitbykojiro/status/1468230003624230928"
    },
    "highlights_xdefi_ama_gotitbykojiro_dec_9_2021": {
        "label": "xdefi_ama_gotitbykojiro_dec_9_2021",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/xdefi_wallet/status/1468913325660459011"
    },
    "highlights_thorswap_partnership_olympusdao": {
        "label": "thorswap_partnership_olympusdao",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/THORSwap/status/1467967367204548608"
    },
    "highlights_thorswap_ama_olympusdao_ninerealms_dec_8_2021": {
        "label": "thorswap_ama_olympusdao_ninerealms_dec_8_2021",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/THORSwap/status/1468678746991431702"
    },
    "highlights_thorswap_changes_summary_dec_11_2021": {
        "label": "thorswap_changes_summary_dec_11_2021",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/THORNOOBs/status/1469586733566025730"
    },
    "highlights_thoryield_transaction_tracker_live": {
        "label": "thoryield_transaction_tracker_live",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/THORYieldApp/status/1468979141152325641"
    },
    "highlights_rango_raises_1m": {
        "label": "rango_raises_1m",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/RangoExchange/status/1471100512682577922"
    },
    "highlights_rango_anyswap_integration": {
        "label": "rango_anyswap_integration",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/RangoExchange/status/1468196251661639681"
    },
    "highlights_thorwallet_how_downloads_increase_tgt_value": {
        "label": "thorwallet_how_downloads_increase_tgt_value",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/ThorWallet/status/1468922420174245892"
    },
    "highlights_thorwallet_user_growth_1_week_apple_store": {
        "label": "thorwallet_user_growth_1_week_apple_store",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/ThorWallet/status/1467446280980938752"
    },
    "highlights_thorwallet_ama_recap_dec_10_2021": {
        "label": "thorwallet_ama_recap_dec_10_2021",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/ThorWallet/status/1469247070070583299"
    },
    "highlights_defispot_ido_rescheduled_q1_2022": {
        "label": "defispot_ido_rescheduled_q1_2022",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/Defi_Spot/status/1468291057389559811"
    },
    "highlights_defispot_spot_will_be_cw_token": {
        "label": "defispot_spot_will_be_cw_token",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/THORNOOBs/status/1471192463129735173"
    },
    "highlights_swapagram_testflight": {
        "label": "swapagram_testflight",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/thorbook/status/1468244288781832193"
    },
    "highlights_asgardex_new_churn_countdown_tool": {
        "label": "asgardex_new_churn_countdown_tool",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/asgardex/status/1470384940126818309"
    },
    "highlights_ferz_wallet_dak_theme_mobile": {
        "label": "ferz_wallet_dak_theme_mobile",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/FerzWallet/status/1470638356035944450"
    },
    "highlights_ferz_wallet_fear_greed_index": {
        "label": "ferz_wallet_fear_greed_index",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/FerzWallet/status/1469520139796336643"
    },
    "highlights_thorguards_nft_weapons_coming": {
        "label": "thorguards_nft_weapons_coming",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/ThorGuards/status/1470847316391182338"
    },
    "highlights_thorguards_archer_giveaway": {
        "label": "thorguards_archer_giveaway",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/ThorGuards/status/1471172578119090178"
    },
    "highlights_thorguards_easter_egg_hunt": {
        "label": "thorguards_easter_egg_hunt",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/ThorGuards/status/1470556904531247110"
    },
    "highlights_thorguards_bnpl_pay_nft_giveaway": {
        "label": "thorguards_bnpl_pay_nft_giveaway",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/ThorGuards/status/1468711676111310849"
    },
    "highlights_thorchads_dao_why_rewarding_its_community": {
        "label": "thorchads_dao_why_rewarding_its_community",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/THORChadsDAO/status/1470714327581794306"
    },
    "highlights_skald_thornoobs_merging_into_thorchain_org": {
        "label": "skald_thornoobs_merging_into_thorchain_org",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/SKALDcamp/status/1470517622370451457"
    },
    "highlights_skald_attribute_testing_begins": {
        "label": "skald_attribute_testing_begins",
        "category": "highlights",
        "action": "external_link",
        "link": "https://twitter.com/SKALDcamp/status/1470871038103855109"
    },
    "highlights_": {
        "label": "",
        "category": "highlights",
        "action": "external_link",
        "link": ""
    },
    // /mobile stories
    "stories_grc_eth_hack": {
        "label": "stories_grc_eth_hack",
        "category": "stories",
        "action": "external_link",
        "link": "https://www.youtube.com/watch?v=g6i94c_nMNY"
    },
    "stories_immunefi_bug_bounty": {
        "label": "stories_immunefi_bug_bounty",
        "category": "stories",
        "action": "external_link",
        "link": "https://immunefi.com/bounty/thorchain/"
    },
    "stories_name_service_thornames_web_app": {
        "label": "stories_thornames_web_app",
        "category": "stories",
        "action": "external_link",
        "link": "https://thorname.com"
    },
    "stories_skip_web_app": {
        "label": "stories_skip_web_app",
        "category": "stories",
        "action": "external_link",
        "link": "https://skip.exchange"
    },
    "stories_xdefi_twitter": {
        "label": "stories_xdefi_twitter",
        "category": "stories",
        "action": "external_link",
        "link": "https://twitter.com/xdefi_wallet"
    },
    "stories_grc_quick_guide_to_mccn": {
        "label": "stories_grc_quick_guide_to_mccn",
        "category": "stories",
        "action": "external_link",
        "link": "https://www.youtube.com/watch?v=a_c5Z2tsSgc"
    },
    "stories_shapeshift": {
        "label": "stories_shapeshift",
        "category": "stories",
        "action": "external_link",
        "link": "https://shapeshift.com/invite"
    },
    "stories_multicoin_thorchain_analysis": {
        "label": "stories_multicoin_thorchain_analysis",
        "category": "stories",
        "action": "external_link",
        "link": "https://multicoin.capital/2021/02/23/thorchain-analysis/"
    },
    "stories_kraken_listed": {
        "label": "stories_kraken_listed",
        "category": "stories",
        "action": "external_link",
        "link": "https://twitter.com/krakenfx/status/1522591141635502080"
    },
    "stories_thorwallet_web_app": {
        "label": "stories_thorwallet_web_app",
        "category": "stories",
        "action": "external_link",
        "link": "https://thorwallet.medium.com/webapp-beta-launch-19d58ffda386"
    },
    // Internal Links
    "slash_arbitrageur_tools": {
      "localLink": "true", 
      "label": '/arbitrageur-tools', 
      "category": 'arbitrageur_tools', 
      "action": 'internal_link',
      "link": '/ecosystem/arbitrageur-tools'
    },
    "slash_data_analyst_tools": {
      "localLink": "true", 
      "label": '/data-analyst-tools', 
      "category": 'data_analyst_tools', 
      "action": 'internal_link',
      "link": '/ecosystem/data-analyst-tools'
    },
    "slash_arbitrageurs": {
      "localLink": "true", 
      "label": '/arbitrageurs', 
      "category": 'community_roles', 
      "action": 'internal_link',
      "link": '/community-roles/arbitrageurs'
    },
    "slash_community_roles": {
      "localLink": "true", 
      "label": '/community-roles', 
      "category": 'community_roles', 
      "action": 'internal_link',
      "link": '/community-roles'
    },
    "slash_daos": {
      "localLink": "true", 
      "label": '/daos', 
      "category": 'daos', 
      "action": 'internal_link',
      "link": '/ecosystem/daos'
    },
    "slash_developers": {
      "localLink": "true", 
      "label": '/developers', 
      "category": 'community_roles', 
      "action": 'internal_link',
      "link": '/community-roles/developers'
    },
    "slash_developers_chain_client": {
      "localLink": "true", 
      "label": '/developers#chain-client', 
      "category": 'community_roles', 
      "action": 'internal_link',
      "link": '/community-roles/developers#chain-client'
    },
    "slash_document_library": {
      "localLink": "true", 
      "label": '/document-library', 
      "category": 'document_library', 
      "action": 'internal_link',
      "link": '/document-library'
    },
    "slash_ecosystem": {
      "localLink": "true", 
      "label": '/ecosystem', 
      "category": 'ecosystem', 
      "action": 'internal_link',
      "link": '/ecosystem'
    },
    "slash_getting_started": {
      "localLink": "true", 
      "label": '/getting-started', 
      "category": 'getting_started', 
      "action": 'internal_link',
      "link": '/getting-started'
    },
    "slash_index": {
      "localLink": "true", 
      "label": '/home', 
      "category": 'home', 
      "action": 'internal_link',
      "link": '/'
    },
    "slash_insurance": {
      "localLink": "true", 
      "label": '/insurance', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/insurance'
    },
    "slash_interfaces": {
      "localLink": "true", 
      "label": '/interfaces', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces'
    },
    "slash_interfaces_links": {
      "localLink": "true", 
      "label": '/interfaces-links', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces-links'
    },
    "slash_interfaces_links_thorswap": {
      "localLink": "true", 
      "label": '/interfaces-links#thorswap', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces-links#thorswap'
    },
    "slash_interfaces_links_asgardex": {
      "localLink": "true", 
      "label": '/interfaces-links#asgardex', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces-links#asgardex'
    },
    "slash_interfaces_links_skip": {
      "localLink": "true", 
      "label": '/interfaces-links#skip', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces-links#skip'
    },
    "slash_interfaces_links_brokkr": {
      "localLink": "true", 
      "label": '/interfaces-links#brokkr', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces-links#brokkr'
    },
    "slash_interfaces_links_shapeshift": {
      "localLink": "true", 
      "label": '/interfaces-links#shapeshift', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces-links#shapeshift'
    },
    "slash_interfaces_links_thorwallet": {
      "localLink": "true", 
      "label": '/interfaces-links#thorwallet', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces-links#thorwallet'
    },
    "slash_interfaces_links_defispot": {
      "localLink": "true", 
      "label": '/interfaces-links#defispot', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces-links#defispot'
    },
    "slash_interfaces_links_rango": {
      "localLink": "true", 
      "label": '/interfaces-links#rango', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces-links#rango'
    },
    "slash_interfaces_links_ferz": {
      "localLink": "true", 
      "label": '/interfaces-links#ferz', 
      "category": 'interfaces', 
      "action": 'internal_link',
      "link": '/ecosystem/interfaces-links#ferz'
    },
    "slash_liquidity_provider_tools": {
      "localLink": "true", 
      "label": '/liquidity-provider-tools', 
      "category": 'liquidity-provider-tools', 
      "action": 'internal_link',
      "link": '/ecosystem/liquidity-provider-tools'
    },
    "slash_liquidity_providers": {
      "localLink": "true", 
      "label": '/liquidity-providers', 
      "category": 'community_roles', 
      "action": 'internal_link',
      "link": '/community-roles/liquidity-providers'
    },
    "slash_network_explorers": {
      "localLink": "true", 
      "label": '/network-explorers', 
      "category": 'ecosystem', 
      "action": 'internal_link',
      "link": '/ecosystem/network-explorers'
    },
    "slash_network": {
      "localLink": "true", 
      "label": '/network', 
      "category": 'network', 
      "action": 'internal_link',
      "link": '/getting-started/network'
    },
    "slash_nft_collections": {
      "localLink": "true", 
      "label": '/nft-collections', 
      "category": 'ecosystem', 
      "action": 'internal_link',
      "link": '/ecosystem/nft-collections'
    },
    "slash_node_operators": {
      "localLink": "true", 
      "label": '/node-operators', 
      "category": 'community_roles', 
      "action": 'internal_link',
      "link": '/community-roles/node-operators'
    },
    "slash_project_funding": {
      "localLink": "true", 
      "label": '/project-funding', 
      "category": 'project_funding', 
      "action": 'internal_link',
      "link": '/ecosystem/project-funding'
    },
    "slash_research_labs": {
      "localLink": "true", 
      "label": '/research-labs', 
      "category": 'research_labs', 
      "action": 'internal_link',
      "link": '/research-labs'
    },
    "slash_rune": {
      "localLink": "true", 
      "label": '/rune', 
      "category": 'rune', 
      "action": 'internal_link',
      "link": '/getting-started/rune'
    },
    "slash_security_solvency": {
      "localLink": "true", 
      "label": '/security-solvency', 
      "category": 'security_solvency', 
      "action": 'internal_link',
      "link": '/security-solvency'
    },
    "slash_technology": {
      "localLink": "true", 
      "label": '/technology', 
      "category": 'technology', 
      "action": 'internal_link',
      "link": '/getting-started/technology'
    },
    "slash_thorfi": {
      "localLink": "true", 
      "label": '/thorfi', 
      "category": 'thorfi', 
      "action": 'internal_link',
      "link": '/getting-started/thorfi'
    },
    "slash_thornames": {
      "localLink": "true", 
      "label": '/thornames', 
      "category": 'name_service', 
      "action": 'internal_link',
      "link": '/ecosystem/thornames'
    },
    "slash_wallets": {
      "localLink": "true", 
      "label": '/wallets', 
      "category": 'wallets', 
      "action": 'internal_link',
      "link": '/ecosystem/wallets'
    },
    "slash_wallets_xdefi": {
      "localLink": "true", 
      "label": '/wallets#xdefi', 
      "category": 'wallets', 
      "action": 'internal_link',
      "link": '/ecosystem/wallets#xdefi'
    },
    "slash_weekly_recap": {
      "localLink": "true", 
      "label": '/weekly', 
      "category": 'wallets', 
      "action": 'internal_link',
      "link": '/recaps/weekly/dec-15-21-2021'
    },
  }
