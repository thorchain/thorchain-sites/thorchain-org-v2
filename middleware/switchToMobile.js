export default function ({route, redirect, $device}) {
    if($device.isMobile){
        if(process.env.mobileBaseUrl){
            redirect(process.env.mobileBaseUrl + route.fullPath)
        }
    }
  }